# "forward 5" adds 5 to X and adds X*aim to depth, "down 5" adds 5 to aim, "up 5" subtracts 5 from aim etc
# Find X*depth

commands = ""

with open("input.txt", "r") as f:
    commands = [x.split() for x in f.readlines()]

horizontal = 0
depth = 0
aim = 0

for command in commands:
    direction = command[0]
    distance = int(command[1].strip())

    if direction == "forward":
        horizontal += distance
        depth += distance*aim
    elif direction == "down":
        aim += distance
    else:
        aim -= distance

with open("part2results.txt", "w") as f:
    f.write(str(horizontal*depth))