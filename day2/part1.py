# "forward 5" adds 5 to X, "down 5" adds 5 to depth, "up 5" subtracts 5 from depth etc
# Find X*depth

commands = ""

with open("input.txt", "r") as f:
    commands = [x.split() for x in f.readlines()]

horizontal = 0
depth = 0

for command in commands:
    direction = command[0]
    distance = int(command[1].strip())

    if direction == "forward":
        horizontal += distance
    elif direction == "down":
        depth += distance
    else:
        depth -= distance

with open("part1results.txt", "w") as f:
    f.write(str(horizontal*depth))