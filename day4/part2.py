# Bingo! Only rows and columns count. Answer is found by summing all unmarked numbers on the last-to-win board and multipying it by the final number called.

lines = []

with open("input.txt", "r") as f:
    data = f.read()
    lines = data.split("\n")

numbers = [int(x) for x in lines[0].split(",")]

boards_base = data.split("\n\n")
boards_base.pop(0)
processed = [b.replace("\n", " ") for b in boards_base]
boards_str = [b.split() for b in processed]
boards = [[int(x) for x in b] for b in boards_str]

called_numbers = []

def check_board(board, nums):
    rows = [board[x*5:(x*5)+5] for x in range(5)]
    for row in rows:
        row_good = True
        for element in row:
            if element not in called_numbers:
                row_good = False
        if row_good:
            return True

    columns = [board[x:x+25:5] for x in range(5)]

    for column in columns:
        column_good = True
        for element in column:
            if element not in called_numbers:
                column_good = False
        if column_good:
            return True
    return False

result = 0

winners = []

done = False

for number in numbers:
    if done:
        break
    called_numbers.append(number)
    for i, board in enumerate(boards):
        if check_board(board, called_numbers):
            if i not in winners:
                winners.append(i)
            if len(winners) < 100:
                continue
            unmarked = [x for x in board if x not in called_numbers]
            result = sum(unmarked)*number
            done = True
            break

with open("part2result.txt", "w") as f:
    f.write(str(result))