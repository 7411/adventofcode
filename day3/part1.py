# For each column, the most common bit (0 or 1) is the next bit of the gamma rate, and the least common is the next bit of the epsilon rate
# Find gamma*epsilon rate (the power consumption)

row_length = 12

columns = []
gamma = ""
epsilon = ""

result = 0

with open("input.txt", "r") as f:
    lines = f.readlines()
    for i in range(row_length):
        if sum([int(line[i]) for line in lines]) > len(lines)/2:
            gamma += "1"
            epsilon += "0"
        else:
            gamma += "0"
            epsilon += "1"
    result = int(gamma, 2)*int(epsilon, 2)

with open("part1results.txt", "w") as f:
    f.write(str(result))