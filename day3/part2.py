# For each column, the remaining rows for oxygen are the ones that contain the majority numeral (or 1 if even.) The minority thus for CO2, or 0 if even.
# Find oxygen*CO2

row_length = 12

result = 0

with open("input.txt", "r") as f:
    lines = f.readlines()
    oxygen_rows = lines.copy()
    co2_rows = lines.copy()
    for i in range(row_length):
        if len(oxygen_rows) > 1:
            if sum([int(line[i]) for line in oxygen_rows]) >= len(oxygen_rows)/2:
                oxygen_rows = [x for x in oxygen_rows if x[i]=="1"]
            else:
                oxygen_rows = [x for x in oxygen_rows if x[i]=="0"]
        if len(co2_rows) > 1:
            if sum([int(line[i]) for line in co2_rows]) >= len(co2_rows)/2:
                co2_rows = [x for x in co2_rows if x[i]=="0"]
            else:
                co2_rows = [x for x in co2_rows if x[i]=="1"]
        print(len(oxygen_rows), len(co2_rows))

    result = int(oxygen_rows[0], 2)*int(co2_rows[0], 2)

with open("part2results.txt", "w") as f:
    f.write(str(result))