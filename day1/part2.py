# Count the number of times a depth measurement increases from the previous measurement

m = ""

with open("input.txt", "r") as f:
    m = [int(x.strip()) for x in f.readlines()]

number_increases = 0

# hacky but I wanted to do this quickly
for i, depth in enumerate(m):
    if i < len(m)-3:
        if depth+m[i+1]+m[i+2] < m[i+1]+m[i+2]+m[i+3]:
            number_increases += 1

with open("part2results.txt", "w") as f:
    f.write(str(number_increases))