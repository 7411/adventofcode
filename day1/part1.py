# Count the number of times a depth measurement increases from the previous measurement

measurements = ""

with open("input.txt", "r") as f:
    measurements = [int(x.strip()) for x in f.readlines()]

number_increases = 0

for i, depth in enumerate(measurements):
    if i < len(measurements)-1:
        if depth < measurements[i+1]:
            number_increases += 1

with open("part1results.txt", "w") as f:
    f.write(str(number_increases))